const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    const body = JSON.parse(event.body || '{}')
    if (!body.idCurtidas) return util.bind(new Error('Enter your code!'))

    await mysql.query('delete from curtidas where idCurtidas=?', [body.idCurtidas])
    return util.bind({})
  } catch (error) {
    return util.bind(error)
  }
}
