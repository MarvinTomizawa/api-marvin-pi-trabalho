const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    
    if (event.pathParameters && event.pathParameters.idCurtidas) {
      const curtidas = await mysql.query('select idCurtidas, date, usuario_id, post_id from curtidas where idCurtidas=?', [event.pathParameters.idCurtidas])
      return util.bind(curtidas.length ? curtidas[0] : {})
    }

    const curtidas = await mysql.query('select idCurtidas, data, usuario_id, post_id from curtidas')
    return util.bind(curtidas)
  } catch (error) {
    return util.bind(error)
  }
}
